import os

# Constantes del juego

# Constantes relacionadas con la pantalla y la velocidad de los fotogramas

ANCHO = 640
ALTO = 480
FPS = 60
TITULO = "Mi primer juego con OOP en PyGame"

# Ruta de las Media del juego

ESCENARIO_FONDO = os.getcwd() + "/GameFiles/Media/Images/fondo.jpg"
DEIDARA = os.getcwd() + "/GameFiles/Media/Images/deidara sprites.png"
PLATAFORMA_IMAGEN = os.getcwd() + "/GameFiles/Media/Images/plataforma.png"
PISO_IMAGEN = os.getcwd() + "/GameFiles/Media/Images/piso.png"

JUMP = os.getcwd() + "/GameFiles/Media/Sounds/jump.ogg"

GRAVEDAD_JUGADOR = 0.8
ACELERACION_JUGADOR = 0.5
FRICCION_JUGADOR = -0.12

# Colores

NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
VERDE = (0, 255, 0)
ROJO = (255, 0, 0)
AZUL = (0, 0, 255)
VIOLETA = (98, 0, 255)
