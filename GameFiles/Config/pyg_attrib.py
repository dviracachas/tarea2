from GameFiles.Logic.g_logic import *

class PyG():
    def __init__(self, ancho=640, alto=400, titulo="PyGame titulo", fps=60):
        pygame.init()
        pygame.display.set_caption(titulo)
        self.ancho = ancho
        self.alto = alto
        self.pantalla = pygame.display.set_mode((self.ancho, self.alto))
        self.reloj = pygame.time.Clock()
        self.fps = fps

    def game_loop(self):

        hecho = False
        juego = Juego()

        # Game loop
        while not hecho:
            # Bucle principal de eventos

            # Procesa los eventos (pulsaciones del teclado, clicks del ratón, etc.)
            hecho = juego.procesar_eventos()

            # Actualiza las posiciones de los objetos y comprueba colisiones
            juego.logica_de_ejecucion()

            # Dibuja el fotograma actual
            juego.actualizar_pantalla(self.pantalla)
            self.reloj.tick(self.fps)

        pygame.quit()



