from GameFiles.Obj.player import *
from GameFiles.Obj.platform import *
from GameFiles.Obj.floor import *


class Juego:
    def __init__(self):
        self.game_over = False
        self.escenario_imagen = pygame.image.load(ESCENARIO_FONDO)
        self.plataformas = pygame.sprite.Group()
        self.protagonista = Protagonista()
        self.fps_clock = 0

        p1 = Plataforma(50, 50)
        self.plataformas.add(p1)
        p2 = Plataforma(300, 250)
        self.plataformas.add(p2)
        p3 = Plataforma(500, 400)
        self.plataformas.add(p3)

        piso = Piso(0, ALTO - 10)
        self.plataformas.add(piso)

    def procesar_eventos(self):
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                return True
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if self.game_over:
                    self.__init__()
            if evento.type == pygame.KEYDOWN:
                if evento.key == pygame.K_UP:
                    self.protagonista.saltar()

        return False

    def logica_de_ejecucion(self):
        if not self.game_over:
            self.protagonista.mover()
            self.fps_clock = self.fps_clock + 1
            if self.fps_clock % 3 == 0:
                self.fps_clock = 0

            # Revisar si el personaje choca con una plataforma
            if self.protagonista.velocidad.y > 0:
                self.protagonista.salto = True
                colisiones = pygame.sprite.spritecollide(self.protagonista, self.plataformas, False)
                if colisiones:
                    self.protagonista.posicion.y = colisiones[0].rect.top + 5
                    self.protagonista.velocidad.y = 0
                    self.protagonista.salto = False

    def actualizar_pantalla(self, pantalla):
        """ Actualiza la pantalla. """
        if self.game_over:
            fuente = pygame.font.SysFont("impact", 25)
            texto = fuente.render("Game Over, haz click para volver a jugar", True, NEGRO)
            centrar_x = (ANCHO // 2) - (texto.get_width() // 2)
            centrar_y = (ALTO // 2) - (texto.get_height() // 2)
            pantalla.blit(texto, [centrar_x, centrar_y])

        else:
            pantalla.blit(self.escenario_imagen, (0, 0))
            self.plataformas.draw(pantalla)
            self.protagonista.draw(pantalla, self.fps_clock)
            self.protagonista.mov = False

        pygame.display.flip()
