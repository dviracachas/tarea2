import pygame

from GameFiles.Config.const import *


class Protagonista(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(DEIDARA)
        self.current_frame = 0
        self.frames = 4
        self.rect = self.image.subsurface([0, 0, 20, 75]).get_rect()
        self.draw_rect = self.rect.move(-28, 0)
        self.direction = True
        self.mov = False
        self.spr_der = {0: (0, 0, 59, 75), 1: (59, 0, 59, 75), 2: (118, 0, 59, 75), 3: (177, 0, 59, 75)}
        self.spr_izq = {0: (0, 75, 58, 75), 1: (59, 75, 58, 75), 2: (117, 75, 53, 75), 3: (170, 75, 58, 75)}
        self.posicion = pygame.math.Vector2(ANCHO // 2, ALTO // 2)
        self.velocidad = pygame.math.Vector2(0, 0)
        self.aceleracion = pygame.math.Vector2(0, 0)
        self.salto = False

    def saltar(self):
        if not self.salto:
            sonido_salto = pygame.mixer.Sound(JUMP)
            sonido_salto.play()
            self.velocidad.y = -20
            self.salto = True

    def mover(self):

        self.aceleracion = pygame.math.Vector2(0, GRAVEDAD_JUGADOR)

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.direction = False
            self.mov = True
            self.aceleracion = self.aceleracion + pygame.math.Vector2(-ACELERACION_JUGADOR, 0)
        if keys[pygame.K_RIGHT]:
            self.direction = True
            self.mov = True
            self.aceleracion = self.aceleracion + pygame.math.Vector2(ACELERACION_JUGADOR, 0)
        if keys[pygame.K_LEFT] and keys[pygame.K_RIGHT]:
            self.mov = False

        # Aplicar friccion
        self.aceleracion.x += self.velocidad.x * FRICCION_JUGADOR
        # Ecuaciones de movimiento
        self.velocidad = self.velocidad + self.aceleracion
        self.posicion = self.posicion + (self.velocidad + 0.5 * self.aceleracion)

        if self.posicion.x > ANCHO:
            self.posicion.x = 0
        if self.posicion.x < 0:
            self.posicion.x = ANCHO

        self.rect.midbottom = self.posicion

    def draw(self, screen, fps_clock):
        if self.direction:
            self.draw_rect = self.rect.move(-28, 0)
            if self.salto:
                screen.blit(self.image.subsurface(self.spr_der[2]), self.draw_rect)
            else:
                if self.mov:
                    if not fps_clock:
                        self.current_frame = self.current_frame + 1
                    if self.current_frame >= self.frames - 1:
                         self.current_frame = 0
                    screen.blit(self.image.subsurface(self.spr_der[self.current_frame]), self.draw_rect)

                else:
                    screen.blit(self.image.subsurface(self.spr_der[3]), self.draw_rect)
        else:
            self.draw_rect = self.rect.move(-10,0)
            if self.salto:
                screen.blit(self.image.subsurface(self.spr_izq[1]), self.draw_rect)
            else:
                if self.mov:
                    if not fps_clock:
                        self.current_frame = self.current_frame + 1
                    if self.current_frame >= self.frames - 1:
                        self.current_frame = 0
                    screen.blit(self.image.subsurface(self.spr_izq[self.current_frame]), self.draw_rect)
                else:
                    screen.blit(self.image.subsurface(self.spr_izq[0]), self.draw_rect)
