import pygame

from GameFiles.Config.const import *


class Plataforma(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load(PLATAFORMA_IMAGEN)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y